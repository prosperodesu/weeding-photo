#*-* coding: utf-8 *-*

from django.db import models
from gallery.controllers import upload_to
from sorl.thumbnail.shortcuts import get_thumbnail

class Album(models.Model):

    ALBUM_TYPE_CHOICES = (
    ('Weeding', 'Свадебный'),
    ('Different', 'Прочее'),
)

    title = models.CharField(max_length=500, verbose_name='Заголовок')
    pub_date = models.DateTimeField(auto_now_add=True, verbose_name='Дата')
    category = models.CharField(max_length=50, default= 'Weeding', choices=ALBUM_TYPE_CHOICES)
    image = models.ImageField(upload_to=upload_to, verbose_name='Фото')

    def get_thumbnail_html(self):
        img = self.image
        img_resize_url = unicode(get_thumbnail(img, '300x300').url)
        html = '<a class="image-picker" href="%s"><img src="%s"/></a>'
        return html % (self.image.url, img_resize_url)
    get_thumbnail_html.short_description = u'Миниатюра'
    get_thumbnail_html.allow_tags = True

    def __unicode__(self):
        return self.title

    class Meta:
        verbose_name = u'Альбом'
        verbose_name_plural = u'Альбом'

class Photo(models.Model):
    pub_date = models.DateTimeField(auto_now_add=True, verbose_name='Дата')
    image = models.ImageField(verbose_name="Фото",
                            upload_to="images/")
    album = models.ForeignKey(Album, verbose_name='Альбом')

    def get_thumbnail_html(self):
        img = self.image
        img_resize_url = unicode(get_thumbnail(img, '300x300').url)
        html = '<a class="image-picker" href="%s"><img src="%s"/></a>'
        return html % (self.image.url, img_resize_url)
    get_thumbnail_html.short_description = u'Миниатюра'
    get_thumbnail_html.allow_tags = True

    class Meta:
        verbose_name = u'Фото'
        verbose_name_plural = u'Фото'

    @staticmethod
    def autocomplete_search_fields():
        return ("id__iexact", "name__icontains",)
