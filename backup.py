import os
import zipfile
from datetime import datetime
from pydrive.auth import GoogleAuth
from pydrive.drive import GoogleDrive

gauth = GoogleAuth()
auth_url = gauth.GetAuthUrl()
code = raw_input(auth_url)
gauth.Auth(code)

drive = GoogleDrive(gauth)


def zipdir(path, zip):
    for root, dirs, files in os.walk(path):
        for file in files:
            zip.write(os.path.join(root, file))

if __name__ == '__main__':
    filename = 'Gallery_backup_' + str(datetime.now()) + '_.zip'
    zipf = zipfile.ZipFile(filename, 'w')
    zipdir('media', zipf)
    zipf.close()
    archive = drive.CreateFile()
    archive.SetContentFile(filename)
    archive.Upload()